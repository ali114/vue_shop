import Vue from 'vue'
import Router from 'vue-router'

// import Login from './../components/Login.vue' 路由懒加载
const Login = () => import(/* webpackChunkName: "login_home_welcome" */ './../components/Login.vue')
const Home = () => import(/* webpackChunkName: "login_home_welcome" */ './../components/Home.vue')
const Welcome = () => import(/* webpackChunkName: "login_home_welcome" */ './../components/Welcome.vue')

const Users = () => import(/* webpackChunkName: "users_rights_roles" */ './../components/user/Users.vue')
const Rights = () => import(/* webpackChunkName: "users_rights_roles" */ './../components/power/Rights.vue')
const Roles = () => import(/* webpackChunkName: "users_rights_roles" */ './../components/power/Roles.vue')

const Cate = () => import(/* webpackChunkName: "cate_params" */ './../components/goods/Cate.vue')
const Params = () => import(/* webpackChunkName: "cate_params" */ './../components/goods/Params.vue')

const GoodsList = () => import(/* webpackChunkName: "goodsList_add" */ './../components/goods/List.vue')
const Add = () => import(/* webpackChunkName: "goodsList_add" */ './../components/goods/Add.vue')

const Order = () => import(/* webpackChunkName: "order_report" */ './../components/order/order.vue')
const Report = () => import(/* webpackChunkName: "order_report" */ './../components/report/Report.vue')

Vue.use(Router)
// 路由
const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children: [
        { path: '/welcome', component: Welcome },
        { path: '/users', component: Users },
        { path: '/rights', component: Rights },
        { path: '/roles', component: Roles },
        { path: '/categories', component: Cate },
        { path: '/params', component: Params },
        { path: '/goods', component: GoodsList },
        { path: '/goods/add', component: Add },
        { path: '/orders', component: Order },
        { path: '/reports', component: Report }
      ]
    }
  ]
})
// 解决报错
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
// 导航守卫 必须有令牌 登录才能访问 没有则强制到登陆页面
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})
export default router
